package com.user.gunungpadang.core;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

public abstract class BaseController {
    public Activity activity;
    protected Fragment fragment;
    protected View view;

    public BaseController() {
    }

    public BaseController(Activity activity) {
        this.activity = activity;
    }

    public BaseController(Activity activity, Fragment fragment, View view) {
        this.activity = activity;
        this.fragment = fragment;
        this.view = view;
    }

    public void goToActivity(Class<?> destinationClass){
        goToActivity(destinationClass, false, false);
    }

    public void goToActivity(Class<?> destinationClass, boolean finish){
        goToActivity(destinationClass, finish, false);
    }

    public void goToActivity(Class<?> destinationClass, boolean finish, boolean clearAll){
        Intent intent = new Intent(activity, destinationClass);
        if (clearAll) intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        goToActivity(intent, finish);
    }

    public void goToActivity(Intent intent){
        goToActivity(intent, false);
    }

    public void goToActivity(Intent intent, boolean isFinish) {
        activity.startActivity(intent);
        if (isFinish) activity.finish();
    }

    public boolean checkPermission(String permission, int permissionCode) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity,
                        new String[]{permission}, permissionCode);
                return false;
            }
        }
        return true;
    }
}
