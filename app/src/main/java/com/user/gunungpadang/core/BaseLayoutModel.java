package com.user.gunungpadang.core;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

public abstract class BaseLayoutModel {
    public Activity activity;
    public Fragment fragment;
    public View view;

    public BaseLayoutModel(Activity activity) {
        this.activity = activity;
        render();
    }

    public BaseLayoutModel(Activity activity, View view, Fragment fragment) {
        this.activity = activity;
        this.view = view;
        this.fragment = fragment;
        render();
    }

    public BaseLayoutModel() {

    }

    public abstract void render();

    public void onLoadData(ProgressBar progressBar, Button btn) {
        progressBar.setVisibility(View.VISIBLE);
        btn.setVisibility(View.GONE);
    }

    public void onFinishData(ProgressBar progressBar, Button btn) {
        progressBar.setVisibility(View.GONE);
        btn.setVisibility(View.VISIBLE);
    }

    public String getStringFrom(EditText edt) {
        return edt.getText().toString();
    }
}
