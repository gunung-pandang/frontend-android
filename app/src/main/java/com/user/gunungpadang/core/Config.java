package com.user.gunungpadang.core;

public class Config {
    public static final String dbname = "mydatabase";
    public static final int version = 1;
    public static final String BASE_URL = "http://localhost:8000/";
    public static final int minPasswordLength = 6;
    public static final int locationCode = 1000;
    public static final int internetCode = 1001;
}
