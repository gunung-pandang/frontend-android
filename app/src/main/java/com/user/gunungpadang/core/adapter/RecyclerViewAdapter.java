package com.user.gunungpadang.core.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import com.user.gunungpadang.core.model.Place;

import java.util.List;


public abstract class RecyclerViewAdapter<T, V extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<V> {

    protected List<T> list;
    protected  Context context;

    protected RecyclerViewAdapter(List<T> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    public void refreshData(List<T> list){
        for (T t : list) {
            Log.d("cekhomecontroller", "refreshData: " + t.toString());
        }
        this.list = list;
        notifyDataSetChanged();
    }
}
