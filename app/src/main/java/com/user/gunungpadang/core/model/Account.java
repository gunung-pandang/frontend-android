package com.user.gunungpadang.core.model;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.user.gunungpadang.core.service.MyDatabase;
import com.user.gunungpadang.feature.register.RegisterController;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
@Table(database = MyDatabase.class)
public class Account extends BaseModel {
    @SerializedName("id")
    @Expose
    @PrimaryKey
    @Column
    private String id;
    @SerializedName("username")
    @Expose
    @Column
    private String username;
    @SerializedName("name")
    @Expose
    @Column
    private String name;
    @SerializedName("password")
    @Expose
    @Column
    private String password;

    public Account() {
    }

    public Account(String username, String name, String password) {
        this.id = null;
        this.username = username;
        this.name = name;
        this.password = password;
    }

    public Account(String id, String username, String name, String password) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.password = password;
    }

    public static Account instanceFromDatabase() {
        return new Select().from(Account.class).querySingle();
    }

    public void registerToFirebase(final RegisterController controller, final DatabaseReference.CompletionListener completionListener) {
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("account/" + username);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()) {
                    Toast.makeText(controller.activity, "Username already exist", Toast.LENGTH_SHORT).show();
                    controller.model.onFinishData(controller.model.progressBar, controller.model.btnRegister);
                } else {
                    mDatabase.removeEventListener(this);
                    id = mDatabase.push().getKey();
                    if (id != null) {
                        mDatabase.child(id).setValue(convertToHashMap(), completionListener);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(controller.activity, "Something was error", Toast.LENGTH_SHORT).show();
                controller.model.onFinishData(controller.model.progressBar, controller.model.btnRegister);
            }
        });
    }

    private Map<String, String> convertToHashMap() {
        Map<String, String> map = new HashMap<>();
        map.put("id", id);
        map.put("username", username);
        map.put("name", name);
        map.put("password", password);
        return map;
    }

    public String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));

            return hexString.toString();
        }catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
