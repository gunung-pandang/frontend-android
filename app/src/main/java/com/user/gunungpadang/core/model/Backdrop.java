package com.user.gunungpadang.core.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.user.gunungpadang.core.service.MyDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Table(database = MyDatabase.class)
public class Backdrop extends BaseModel {
    @SerializedName("id")
    @Expose
    @PrimaryKey
    @Column
    private String id;
    @SerializedName("img_url")
    @Expose
    @Column
    private String imgurl;

    public Backdrop() {
    }

    public Backdrop(String id, String imgurl) {
        this.id = id;
        this.imgurl = imgurl;
    }

    public static List<Backdrop> loadBackdropsFromDatabase() {
        return new Select().from(Backdrop.class).queryList();
    }

    public static void dummyBackdropToFirebase() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("backdrop");
        List<String> list = new ArrayList<>();
        list.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLfHLekNtXFq7J68c0_RFQENz5VKssVbk0Ni3Z_EmZykqH1uFRYA");
        list.add("https://upload.wikimedia.org/wikipedia/commons/a/af/Gunung_Padang_5th_terrace.jpeg");
        list.add("http://2.bp.blogspot.com/-8iz3KB_otG8/WKXLnQTlHlI/AAAAAAAAAng/WMCLfM1W02waYoMwDqP3GICwAVQWKcApgCK4B/s1600/sokogunung-1487178693913.jpg");
        list.add("https://situsgunungpadang.com/web_component/gallery/ca10bf3442d6123b6ce49c3d336ee717.jpg");
        list.add("https://asset.kompas.com/data/photo/2014/06/06/1019537GunungPadang021402020265-preview780x390.jpg");

        for (int i = 0; i < list.size(); i++) {
            Backdrop backdrop = new Backdrop(databaseReference.push().getKey(), list.get(i));
            databaseReference.child(backdrop.id).setValue(backdrop.covertToMipmap(), new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    System.out.println("added");
                }
            });
        }
    }

    private HashMap<String, Object> covertToMipmap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", id);
        hashMap.put("imgurl", imgurl);
        return hashMap;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }
}
