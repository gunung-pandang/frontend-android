package com.user.gunungpadang.core.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.user.gunungpadang.core.service.MyDatabase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Table(database = MyDatabase.class)
public class Place extends BaseModel {
    @SerializedName("id")
    @Expose
    @PrimaryKey
    @Column
    private String id;
    @SerializedName("name")
    @Expose
    @Column
    private String name;
    @SerializedName("info")
    @Expose
    @Column
    private String info;
    @SerializedName("latitude")
    @Expose
    @Column
    private double latitude;
    @SerializedName("longitude")
    @Expose
    @Column
    private double longitude;
    @SerializedName("photos")
    @Expose
    @Column
    private String photos;
    @SerializedName("icon")
    @Expose
    @Column
    private String icon;
    @SerializedName("maps_icon")
    @Expose
    @Column
    private String mapsIcon;

    private List<String> listPhoto;

    public Place() {
    }

    public Place(String id, String name, String info, double latitude, double longitude, List<String> listPhoto, String icon) {
        this.id = id;
        this.name = name;
        this.info = info;
        this.latitude = latitude;
        this.longitude = longitude;
        this.listPhoto = listPhoto;
        this.icon = icon;
        this.photos = listToString(listPhoto);
        this.mapsIcon = "";
    }

    public Place(String id, String name, String info, double latitude, double longitude, String icon, String mapsIcon, List<String> listPhoto) {
        this.id = id;
        this.name = name;
        this.info = info;
        this.latitude = latitude;
        this.longitude = longitude;
        this.icon = icon;
        this.mapsIcon = mapsIcon;
        this.listPhoto = listPhoto;
        this.photos = listToString(listPhoto);
    }

    public String getSpoilerInfo() {
        if (info.length() < 50) return info;
        else return info.substring(0, 50) + "...";
    }

    public static List<String> stringToList(String list) {
        return Arrays.asList(list.split(","));
    }

    public String listToString(List<String> list) {
        String result = "";
        for (String s : list) result += s + ",";
        return result;
    }

    public static Place loadFromDatabase() {
        Place place = new Select().from(Place.class).querySingle();
        if (place != null) place.setListPhoto(stringToList(place.getPhotos()));
        return place;
    }

    public static List<Place> loadPlacesFromDatabase() {
        return new Select().from(Place.class).queryList();
    }

    public static void dummyDataToFirebase() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("place");

        List<String> list = new ArrayList<>();
        list.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLfHLekNtXFq7J68c0_RFQENz5VKssVbk0Ni3Z_EmZykqH1uFRYA");
        list.add("https://upload.wikimedia.org/wikipedia/commons/a/af/Gunung_Padang_5th_terrace.jpeg");
        list.add("http://2.bp.blogspot.com/-8iz3KB_otG8/WKXLnQTlHlI/AAAAAAAAAng/WMCLfM1W02waYoMwDqP3GICwAVQWKcApgCK4B/s1600/sokogunung-1487178693913.jpg");
        list.add("https://situsgunungpadang.com/web_component/gallery/ca10bf3442d6123b6ce49c3d336ee717.jpg");
        list.add("https://asset.kompas.com/data/photo/2014/06/06/1019537GunungPadang021402020265-preview780x390.jpg");

        List<Place> listPlace = new ArrayList<>();
        listPlace.add(new Place("1", "Teras Gunung Pandang", "Teras yang sudah ada sejak tahun 50 ribuan", -6.993733,107.056354, list, "https://pbs.twimg.com/profile_images/3589753105/935aeebb265a30263a52e3e6e92603a6.jpeg"));
        listPlace.add(new Place("2", "Teras Gunung Pandang", "Teras yang sudah ada sejak tahun 50 ribuan", -6.993533,107.056154, list, "https://pbs.twimg.com/profile_images/3589753105/935aeebb265a30263a52e3e6e92603a6.jpeg"));
        listPlace.add(new Place("3", "Teras Gunung Pandang", "Teras yang sudah ada sejak tahun 50 ribuan", -6.993533,107.056354, list, "https://pbs.twimg.com/profile_images/3589753105/935aeebb265a30263a52e3e6e92603a6.jpeg"));
        listPlace.add(new Place("4", "Teras Gunung Pandang", "Teras yang sudah ada sejak tahun 50 ribuan", -6.993733,107.056154, list, "https://pbs.twimg.com/profile_images/3589753105/935aeebb265a30263a52e3e6e92603a6.jpeg"));

        for (Place place : listPlace) {
            place.id = databaseReference.push().getKey();
            databaseReference.child(place.id).setValue(place.covertToMipmap(), new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    System.out.println("added");
                }
            });
        }
    }

    private HashMap<String, Object> covertToMipmap() {
        HashMap<String, Object> hashMap  = new HashMap<>();
        hashMap.put("id", id);
        hashMap.put("name", name);
        hashMap.put("info", info);
        hashMap.put("latitude", latitude);
        hashMap.put("longitude", longitude);
        hashMap.put("photos", listPhoto);
        hashMap.put("icon", icon);
        hashMap.put("maps_icon", mapsIcon);
        return hashMap;
    }

    @Override
    public String toString() {
        String s = "";
        s += "id : " + id + "\n";
        s += "name : " + name + "\n";
        s += "info : " + info + "\n";
        s += "latitude : " + latitude + "\n";
        s += "longitude : " + longitude + "\n";
        s += "photos : " + photos + "\n";
        s += "icon : " + icon + "\n";
        s += "maps_icon : " + mapsIcon + "\n";
        return s;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public void setListPhoto(List<String> listPhoto) {
        this.listPhoto = listPhoto;
    }

    public List<String> getListPhoto() {
        return listPhoto;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMapsIcon() {
        return mapsIcon;
    }

    public void setMapsIcon(String mapsIcon) {
        this.mapsIcon = mapsIcon;
    }
}
