package com.user.gunungpadang.core.service;

import com.raizlabs.android.dbflow.annotation.Database;
import com.user.gunungpadang.core.Config;

/**
 * Created by User on 23/08/2017.
 * @author Ahmad Faiz Sahupala
 *
 * Kelas untuk menginisiasi dbFlow atau database lokal
 */
@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION)
public class MyDatabase {
    public static final String NAME = Config.dbname;

    public static final int VERSION = Config.version;
}
