package com.user.gunungpadang.core.service.retrofit;
import com.user.gunungpadang.core.service.ApiServices;

public class RetrofitServices {
    public static ApiServices sendRequest() {
        return RetrofitClient.client().create(ApiServices.class);
    }
}
