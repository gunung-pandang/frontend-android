package com.user.gunungpadang.feature.backdrop;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.user.gunungpadang.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BackdropFragment extends Fragment {


    public BackdropFragment() {
        // Required empty public constructor
    }

    public static BackdropFragment newInstance(String imgResource) {

        Bundle args = new Bundle();

        BackdropFragment fragment = new BackdropFragment();
        args.putString("img", imgResource);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_backdrop, container, false);
        ImageView img = v.findViewById(R.id.img_backdrop);
        String idResource = getArguments().getString("img");
        Picasso.with(getContext()).load(idResource).into(img);
        return v;
    }

}
