package com.user.gunungpadang.feature.home;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.user.gunungpadang.R;
import com.user.gunungpadang.core.Config;

public class HomeActivity extends AppCompatActivity {

    public HomeController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        int position = getIntent().getIntExtra("position", 0);
        controller = new HomeController(this);

        boolean allowed = controller.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, Config.locationCode);

        if (allowed) {
            controller.createTab(position, getSupportFragmentManager());
            controller.callPlaces();
            controller.callBackdrops();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        controller.permissionResponse(requestCode, grantResults);
    }
}
