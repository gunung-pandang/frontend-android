package com.user.gunungpadang.feature.home;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.user.gunungpadang.core.BaseController;
import com.user.gunungpadang.core.Config;
import com.user.gunungpadang.core.adapter.ViewPagerAdapter;
import com.user.gunungpadang.core.model.Backdrop;
import com.user.gunungpadang.core.model.Place;
import com.user.gunungpadang.feature.home.main.MainFragment;
import com.user.gunungpadang.feature.home.maps.MapsFragment;
import com.user.gunungpadang.feature.home.profile.ProfileFragment;

import java.util.ArrayList;
import java.util.List;

public class HomeController extends BaseController {

    private HomeModel model;
    private ViewPagerAdapter adapter;
    public List<Place> listPlace = new ArrayList<>();
    public List<Backdrop> listBackdrop = new ArrayList<>();

    HomeController(Activity activity) {
        super(activity);
        this.model = new HomeModel(activity);
    }

    void callBackdrops() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("backdrop");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    listBackdrop.add(ds.getValue(Backdrop.class));
                }
                MainFragment mainFragment = (MainFragment) adapter.getItem(0);
                if (mainFragment.controller != null) mainFragment.controller.initializeBackdropAdapter();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(activity, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void callPlaces() {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("place");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Place place = ds.getValue(Place.class);
                    place.setListPhoto(Place.stringToList(place.getPhotos()));
                    listPlace.add(place);
                }
                MainFragment mainFragment = (MainFragment) adapter.getItem(0);
                if (mainFragment.controller != null) mainFragment.controller.refresh();

                MapsFragment mapsFragment = (MapsFragment) adapter.getItem(1);
                if (mapsFragment.controller != null) {
                    mapsFragment.controller.listPlace = listPlace;
                    mapsFragment.controller.addPlaces(listPlace);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(activity, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void createTab(int position, FragmentManager fragmentManager) {
        adapter = new ViewPagerAdapter(fragmentManager);

        adapter.addFragment(new MainFragment(), null);
        adapter.addFragment(new MapsFragment(), null);
        adapter.addFragment(new ProfileFragment(), null);

        model.customizeTab(adapter, position);

    }

    void permissionResponse(int requestCode, int[] grantResults) {
        if (requestCode == Config.locationCode){
            if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)){
                Toast.makeText(activity, "Location Allowed", Toast.LENGTH_SHORT).show();
                goToActivity(HomeActivity.class, true);

            }else{
                Toast.makeText(activity, "Permission Denied to access your Location", Toast.LENGTH_SHORT).show();
                activity.finish();
            }
        }
    }
}
