package com.user.gunungpadang.feature.home;

import android.app.Activity;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.user.gunungpadang.R;
import com.user.gunungpadang.core.BaseLayoutModel;
import com.user.gunungpadang.core.adapter.ViewPagerAdapter;

public class HomeModel extends BaseLayoutModel {

    private TabLayout tabs;
    private ViewPager pager;


    HomeModel(Activity activity) {
        super(activity);
    }

    @Override
    public void render() {
        tabs = activity.findViewById(R.id.tabs);
        pager = activity.findViewById(R.id.pager);
    }

    void customizeTab(ViewPagerAdapter adapter, int position) {
        pager.setAdapter(adapter);
        pager.setCurrentItem(position);
        tabs.setupWithViewPager(pager);
        tabs.setTabTextColors(activity.getResources().getColor(R.color.white),
                activity.getResources().getColor(R.color.white));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
        tabs.setTabMode(TabLayout.MODE_FIXED);

        int[] tabIcon = {
                R.drawable.ic_home,
                R.drawable.ic_maps,
                R.drawable.ic_profile
        };

        for (int i = 0; i < tabIcon.length; i++) {
            TabLayout.Tab tab = tabs.getTabAt(i);
            if (tab != null) tab.setIcon(tabIcon[i]);
        }
    }
}
