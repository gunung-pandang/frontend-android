package com.user.gunungpadang.feature.home.main;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.user.gunungpadang.core.BaseController;
import com.user.gunungpadang.core.adapter.ViewPagerAdapter;
import com.user.gunungpadang.core.model.Backdrop;
import com.user.gunungpadang.core.model.Place;
import com.user.gunungpadang.feature.backdrop.BackdropFragment;
import com.user.gunungpadang.feature.home.HomeActivity;

import java.util.ArrayList;
import java.util.List;

public class MainController extends BaseController {

    private MainModel model;
    ViewPagerAdapter adapter;
    PlaceAdapter placeAdapter;

    private Runnable runnable;
    private Handler handler;

    private int backdropIndex = 0;
    private final int CHANGE_DURATION = 5000;

    public List<Place> listPlace = new ArrayList<>();

    MainController(Activity activity, Fragment fragment, View view) {
        super(activity, fragment, view);
        this.model = new MainModel(activity, view, fragment, this);
    }

    public void initializeBackdropAdapter() {
        final List<Backdrop> listBackdrop = ((HomeActivity)activity).controller.listBackdrop;
        if (listBackdrop.size() != 0) {
            adapter = new ViewPagerAdapter(fragment.getChildFragmentManager());
            for (Backdrop backdrop : listBackdrop) adapter.addFragment(BackdropFragment.newInstance(backdrop.getImgurl()), "");
            model.customizePager();
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    backdropIndex = (backdropIndex+ 1) % listBackdrop.size();
                    model.pager.setCurrentItem(backdropIndex, true);
                    handler.postDelayed(runnable,CHANGE_DURATION);
                }
            };
            handler.postDelayed(runnable,CHANGE_DURATION);
        }
    }

    public void initializePlacesAdapter() {
        placeAdapter = new PlaceAdapter(listPlace, fragment.getContext(), this);
        model.customizeRecyclerView();
    }

    void refreshBackdrop(int position) {
        backdropIndex = position;
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable,CHANGE_DURATION);
    }

    public void refresh() {
        listPlace = ((HomeActivity)activity).controller.listPlace;
        placeAdapter.refreshData(listPlace);
    }
}
