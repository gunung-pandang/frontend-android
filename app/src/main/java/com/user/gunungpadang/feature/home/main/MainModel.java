package com.user.gunungpadang.feature.home.main;

import android.app.Activity;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.user.gunungpadang.R;
import com.user.gunungpadang.core.BaseLayoutModel;

public class MainModel extends BaseLayoutModel {

    private MainController controller;

    ViewPager pager;
    private TabLayout tabLayout;
    private RecyclerView recyclerView;

    MainModel(Activity activity, View view, Fragment fragment, MainController controller) {
        super(activity, view, fragment);
        this.controller = controller;
    }

    @Override
    public void render() {
        pager = view.findViewById(R.id.pager_backdrop);
        tabLayout = view.findViewById(R.id.tabs_dot);
        recyclerView = view.findViewById(R.id.rv_places);
    }

    void customizePager() {
        pager.setAdapter(controller.adapter);
        tabLayout.setupWithViewPager(pager);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                controller.refreshBackdrop(position);
            }

            @Override
            public void onPageSelected(int position) {
                controller.refreshBackdrop(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    void customizeRecyclerView() {
        recyclerView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(fragment.getContext()) {
            @Override
            public boolean canScrollVertically(){
                return false;
            }
        };
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(controller.placeAdapter);
    }
}
