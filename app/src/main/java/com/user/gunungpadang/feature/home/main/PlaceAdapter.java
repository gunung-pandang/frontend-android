package com.user.gunungpadang.feature.home.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.user.gunungpadang.R;
import com.user.gunungpadang.core.adapter.RecyclerViewAdapter;
import com.user.gunungpadang.core.model.Place;
import com.user.gunungpadang.feature.place.PlaceActivity;

import java.util.List;

public class PlaceAdapter extends RecyclerViewAdapter<Place, PlaceAdapter.ViewHolder> {

    private MainController controller;

    public PlaceAdapter(List<Place> list, Context context, MainController controller) {
        super(list, context);
        this.controller = controller;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView txtTitle;
        TextView txtInfo;
        ViewHolder(View v) {
            super(v);
            img = v.findViewById(R.id.img_place);
            txtTitle = v.findViewById(R.id.txt_place);
            txtInfo = v.findViewById(R.id.txt_info_place);
        }
    }

    @Override
    public PlaceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_place,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    @Override
    public void onBindViewHolder(PlaceAdapter.ViewHolder holder, int position) {
        final Place place = list.get(position);

        holder.txtTitle.setText(place.getName());
        holder.txtInfo.setText(place.getSpoilerInfo());
        Picasso.with(context).load(place.getIcon()).into(holder.img);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                place.save();
                controller.goToActivity(PlaceActivity.class);
            }
        });
    }

    @Override
    public void refreshData(List<Place> list){
        this.list = list;
        notifyDataSetChanged();
    }
}
