package com.user.gunungpadang.feature.home.maps;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.user.gunungpadang.core.BaseController;
import com.user.gunungpadang.core.model.Place;
import com.user.gunungpadang.feature.home.HomeActivity;
import com.user.gunungpadang.feature.place.PlaceActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapsController extends BaseController {

    MapsModel mapsModel;
    private GoogleMap googleMap;
    private HashMap<Marker, Place> hashMap = new HashMap<>();
    public List<Place> listPlace;

    MapsController(Activity activity, Fragment fragment, View view) {
        super(activity, fragment, view);
        this.mapsModel = new MapsModel(activity, view, fragment, this);
        this.listPlace = Place.loadPlacesFromDatabase();
    }

    void initializeMaps(Bundle savedInstanceState) {
        mapsModel.mapView.onCreate(savedInstanceState);
        mapsModel.mapView.onResume();
        try {
            MapsInitializer.initialize(activity.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapsModel.mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(fragment.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(fragment.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map
                LatLng sydney = new LatLng(-6.993633,107.056254);
                MarkerOptions markerOptions = new MarkerOptions().position(sydney).title("Gunung Padang").snippet("Gunung Padang");
                final Marker mainMarker = googleMap.addMarker(markerOptions);
                // For zooming automatically to the location of the marker
                final CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(18).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        if (!marker.equals(mainMarker)) {
                            Place place = hashMap.get(marker);
                            place.save();
                            goToActivity(PlaceActivity.class);
                        }
                    }
                });

                googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        return true;
                    }
                });

                googleMap.setOnMyLocationClickListener(new GoogleMap.OnMyLocationClickListener() {
                    @Override
                    public void onMyLocationClick(@NonNull Location location) {
                        Location loc = googleMap.getMyLocation();
                        LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(18).build();
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                });

                listPlace = ((HomeActivity)activity).controller.listPlace;
                addPlaces(listPlace);
            }
        });
    }

    public void addPlaces(List<Place> list) {

        List<Float> listMarker = new ArrayList<>();
        listMarker.add(BitmapDescriptorFactory.HUE_BLUE);
        listMarker.add(BitmapDescriptorFactory.HUE_CYAN);
        listMarker.add(BitmapDescriptorFactory.HUE_GREEN);
        listMarker.add(BitmapDescriptorFactory.HUE_ORANGE);
        listMarker.add(BitmapDescriptorFactory.HUE_ROSE);
        listMarker.add(BitmapDescriptorFactory.HUE_YELLOW);
        listMarker.add(BitmapDescriptorFactory.HUE_MAGENTA);
        listMarker.add(BitmapDescriptorFactory.HUE_AZURE);

        if (googleMap != null) {
            for (Place place : list) {
                Float markerColor = listMarker.remove(0);
                listMarker.add(markerColor);
                MarkerOptions markerOptions = new MarkerOptions().
                        position(new LatLng(place.getLatitude(), place.getLongitude())).
                        title(place.getName()).
                        snippet(place.getName()).
                        icon(BitmapDescriptorFactory.defaultMarker(markerColor));
                Marker marker = googleMap.addMarker(markerOptions);
                hashMap.put(marker, place);
            }
        }
    }
}
