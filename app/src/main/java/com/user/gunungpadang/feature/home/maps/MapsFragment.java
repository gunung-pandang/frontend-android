package com.user.gunungpadang.feature.home.maps;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.user.gunungpadang.R;
import com.user.gunungpadang.core.model.Place;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapsFragment extends Fragment {

    public MapsController controller;

    public MapsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_maps, container, false);
        controller = new MapsController(getActivity(), this, v);
        controller.initializeMaps(savedInstanceState);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        controller.mapsModel.mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        controller.mapsModel.mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        controller.mapsModel.mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        controller.mapsModel.mapView.onLowMemory();
    }
}
