package com.user.gunungpadang.feature.home.maps;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

import com.google.android.gms.maps.MapView;
import com.user.gunungpadang.R;
import com.user.gunungpadang.core.BaseLayoutModel;

public class MapsModel extends BaseLayoutModel {

    MapsController controller;
    MapView mapView;

    public MapsModel(Activity activity, View view, Fragment fragment, MapsController controller) {
        super(activity, view, fragment);
        this.controller = controller;
    }

    @Override
    public void render() {
        mapView = view.findViewById(R.id.map_view);
    }
}
