package com.user.gunungpadang.feature.home.profile;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;

import com.user.gunungpadang.core.BaseController;
import com.user.gunungpadang.core.model.Account;
import com.user.gunungpadang.feature.home.HomeActivity;

public class ProfileController extends BaseController {

    private ProfileModel model;
    private Account account = Account.instanceFromDatabase();

    ProfileController(Activity activity, Fragment fragment, View view) {
        super(activity, fragment, view);
        this.model = new ProfileModel(activity, view, fragment, this);
    }

    public void logout() {
        account.delete();
        Intent intent = new Intent(activity, HomeActivity.class);
        intent.putExtra("position", 2);
        goToActivity(intent, true);
    }

    void displayAccount() {
        if (account == null) model.hideMyProfile();
        else model.showMyProfile(account);
    }
}
