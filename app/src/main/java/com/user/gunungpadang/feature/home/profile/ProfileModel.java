package com.user.gunungpadang.feature.home.profile;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.user.gunungpadang.R;
import com.user.gunungpadang.core.BaseLayoutModel;
import com.user.gunungpadang.core.model.Account;
import com.user.gunungpadang.feature.login.LoginActivity;
import com.user.gunungpadang.feature.register.RegisterActivity;

public class ProfileModel extends BaseLayoutModel {

    private ProfileController controller;

    private TextView txtName;
    private TextView txtUsername;
    private Button btnLogout;
    private Button btnLogin;
    private Button btnRegister;
    private LinearLayout boxMyProfile;
    private LinearLayout boxNoProfile;

    ProfileModel(Activity activity, View view, Fragment fragment, ProfileController controller) {
        super(activity, view, fragment);
        this.controller = controller;
    }

    void showMyProfile(Account account) {
        boxMyProfile.setVisibility(View.VISIBLE);
        boxNoProfile.setVisibility(View.GONE);
        txtName.setText(account.getName());
        txtUsername.setText(account.getUsername());
    }

    void hideMyProfile() {
        boxMyProfile.setVisibility(View.GONE);
        boxNoProfile.setVisibility(View.VISIBLE);
    }

    @Override
    public void render() {
        txtName = view.findViewById(R.id.txt_name);
        txtUsername = view.findViewById(R.id.txt_username);
        btnLogout = view.findViewById(R.id.btn_logout);
        btnLogin = view.findViewById(R.id.btn_login);
        btnRegister = view.findViewById(R.id.btn_register);
        boxMyProfile = view.findViewById(R.id.box_my_profile);
        boxNoProfile = view.findViewById(R.id.box_no_profile);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.logout();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.goToActivity(LoginActivity.class);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.goToActivity(RegisterActivity.class);
            }
        });
    }
}
