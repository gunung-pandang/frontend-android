package com.user.gunungpadang.feature.login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.user.gunungpadang.R;

public class LoginActivity extends AppCompatActivity {

    LoginController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        controller = new LoginController(this);
    }
}
