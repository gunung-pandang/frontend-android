package com.user.gunungpadang.feature.login;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.user.gunungpadang.R;
import com.user.gunungpadang.core.BaseController;
import com.user.gunungpadang.core.Config;
import com.user.gunungpadang.core.model.Account;
import com.user.gunungpadang.feature.home.HomeActivity;

class LoginController extends BaseController {
    private LoginModel model;

    LoginController(Activity activity) {
        super(activity);
        this.model = new LoginModel(activity, this);
    }

    private boolean passwordConfirmed() {
        return model.getStringFrom(model.edtPassword).length() >= Config.minPasswordLength;
    }

    public void login() {
        model.onLoadData();
        if (!passwordConfirmed()) {
            Toast.makeText(activity, R.string.must_6_char, Toast.LENGTH_SHORT).show();
            model.onFinishData();
        }
        else {
            final Account account = new Account();
            account.setUsername(model.getStringFrom(model.edtUsername));
            account.setPassword(account.md5(model.getStringFrom(model.edtPassword)));

            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("account/" + account.getUsername());
            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Account acc = null;
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        acc = ds.getValue(Account.class);
                    }

                    if (acc != null) {
                        if (!acc.getPassword().equals(account.getPassword())){
                            Toast.makeText(activity, "Wrong Password", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "Success", Toast.LENGTH_SHORT).show();
                            acc.save();
                            goToActivity(HomeActivity.class, true, true);
                        }
                    } else {
                        Toast.makeText(activity, "Account not found", Toast.LENGTH_SHORT).show();
                    }
                    model.onFinishData();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(activity, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    model.onFinishData();
                }
            });

        }
    }
}
