package com.user.gunungpadang.feature.login;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.user.gunungpadang.R;
import com.user.gunungpadang.core.BaseLayoutModel;
import com.user.gunungpadang.core.Config;
import com.user.gunungpadang.feature.register.RegisterActivity;

public class LoginModel extends BaseLayoutModel {

    private LoginController controller;
    EditText edtUsername;
    EditText edtPassword;
    private Button btnLogin;
    private ProgressBar progressLogin;
    private TextView txtRegister;
    
    public LoginModel(Activity activity, LoginController controller) {
        super(activity);
        this.controller = controller;
    }

    void onLoadData() {
        super.onLoadData(progressLogin, btnLogin);
    }

    void onFinishData() {
        super.onFinishData(progressLogin, btnLogin);
    }

    @Override
    public void render() {
        edtUsername = activity.findViewById(R.id.edt_username);
        edtPassword = activity.findViewById(R.id.edt_password);
        btnLogin = activity.findViewById(R.id.btn_login);
        progressLogin = activity.findViewById(R.id.progress_login);
        txtRegister = activity.findViewById(R.id.txt_register);
        
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.login();

            }
        });
        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.goToActivity(RegisterActivity.class);
            }
        });
    }
}
