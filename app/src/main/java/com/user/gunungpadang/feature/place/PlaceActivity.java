package com.user.gunungpadang.feature.place;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.user.gunungpadang.R;
import com.user.gunungpadang.core.model.Place;
import com.user.gunungpadang.feature.place.struct.Review;

import java.util.ArrayList;
import java.util.List;

public class PlaceActivity extends AppCompatActivity {

    PlaceController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place);

        controller = new PlaceController(this);
        setSupportActionBar(controller.model.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.detail);
        controller.setPlaceData(getSupportFragmentManager());
        controller.callReview();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
