package com.user.gunungpadang.feature.place;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.user.gunungpadang.R;
import com.user.gunungpadang.core.BaseController;
import com.user.gunungpadang.core.adapter.ViewPagerAdapter;
import com.user.gunungpadang.core.model.Account;
import com.user.gunungpadang.core.model.Place;
import com.user.gunungpadang.feature.backdrop.BackdropFragment;
import com.user.gunungpadang.feature.place.struct.Review;

import java.util.ArrayList;
import java.util.List;

class PlaceController extends BaseController {
    PlaceModel model;
    ViewPagerAdapter adapter;
    ReviewAdapter reviewAdapter;
    private Place place;
    private Account account = Account.instanceFromDatabase();

    private Handler handler;
    private Runnable runnable;
    private int backdropIndex = 0;
    private final int CHANGE_DURATION = 5000;
    private boolean reviewed = false;

    PlaceController(Activity activity) {
        super(activity);
        this.model = new PlaceModel(activity, this);
        place = Place.loadFromDatabase();
        place.delete();
    }

    void setPlaceData(FragmentManager fragmentManager) {
        if (place == null) activity.finish();
        else {
            initializeBackdropAdapter(place.getListPhoto(), fragmentManager);
            model.displayPlace(place);
        }
    }

    private void initializeBackdropAdapter(final List<String> list, FragmentManager fragmentManager) {
        adapter = new ViewPagerAdapter(fragmentManager);
        for (String url : list) adapter.addFragment(BackdropFragment.newInstance(url), "");
        model.customizePager();
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                backdropIndex = (backdropIndex+ 1) % list.size();
                model.pager.setCurrentItem(backdropIndex, true);
                handler.postDelayed(runnable,CHANGE_DURATION);
            }
        };
        handler.postDelayed(runnable,CHANGE_DURATION);
    }

    private void initializeReviewAdapter(final List<Review> list) {
        reviewAdapter = new ReviewAdapter(list, activity.getApplicationContext());
        model.customizeRecyclerView();
    }

    void refreshBackdrop(int position) {
        backdropIndex = position;
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable,CHANGE_DURATION);
    }

    void findLocation() {
        String data = "geo:"
                + String.valueOf(place.getLatitude())
                + "," + String.valueOf(place.getLongitude())
                + "?q=" + String.valueOf(place.getLatitude())
                + "," + String.valueOf(place.getLongitude())
                + "(" + place.getName() + ")";
        Uri gmmIntentUri = Uri.parse(data);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        goToActivity(mapIntent);
    }

    void review() {
        if (account == null) Toast.makeText(activity, R.string.login_first_to_review, Toast.LENGTH_SHORT).show();
        else if (reviewed) Toast.makeText(activity, R.string.already_review_this_place, Toast.LENGTH_SHORT).show();
        else {
            model.displayReviewLayout(true);
        }
    }

    void submitReview() {
        String review = model.getStringFrom(model.edtReview);
        if (review.length() == 0) Toast.makeText(activity, R.string.must_fill_review, Toast.LENGTH_SHORT).show();
        else {
            model.onLoadData(model.progressReview, model.btnSubmitReview);
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("review/" + place.getId());
            Review rvw = new Review();
            rvw.setId(databaseReference.push().getKey());
            rvw.setReview(review);
            rvw.setUsername(account.getUsername());
            databaseReference.child(rvw.getId()).setValue(rvw.convertToMipmap(), new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    model.onFinishData(model.progressReview, model.btnSubmitReview);
                    Toast.makeText(activity, "Success!", Toast.LENGTH_SHORT).show();
                    place.save();
                    goToActivity(PlaceActivity.class, true);
                }
            });
        }
    }

    void callReview() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("review/" + place.getId());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Review> list = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Review rv = ds.getValue(Review.class);
                    list.add(rv);
                    if (rv != null && account != null && rv.getUsername().equals(account.getUsername()))
                        reviewed = true;
                }
                initializeReviewAdapter(list);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(activity, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
