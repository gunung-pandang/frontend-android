package com.user.gunungpadang.feature.place;

import android.app.Activity;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.user.gunungpadang.R;
import com.user.gunungpadang.core.BaseLayoutModel;
import com.user.gunungpadang.core.model.Place;

public class PlaceModel extends BaseLayoutModel {

    PlaceController controller;

    Toolbar toolbar;
    ViewPager pager;
    TabLayout tabLayout;
    TextView txtTitle;
    LinearLayout boxFindMe;
    TextView txtInfo;
    ImageView imgIcon;
    Button btnReview;
    RecyclerView rvReviews;

    RelativeLayout reviewLayout;
    EditText edtReview;
    Button btnSubmitReview;
    ProgressBar progressReview;


    public PlaceModel(Activity activity, PlaceController controller) {
        super(activity);
        this.controller = controller;
    }

    @Override
    public void render() {
        toolbar = activity.findViewById(R.id.toolbar);
        pager = activity.findViewById(R.id.pager_backdrop);
        tabLayout = activity.findViewById(R.id.tabs_dot);
        txtTitle = activity.findViewById(R.id.txt_title);
        boxFindMe = activity.findViewById(R.id.box_find_me);
        txtInfo = activity.findViewById(R.id.txt_info);
        imgIcon = activity.findViewById(R.id.img_icon);
        btnReview = activity.findViewById(R.id.btn_review);
        rvReviews = activity.findViewById(R.id.rv_review);
        progressReview = activity.findViewById(R.id.progress_review);

        reviewLayout = activity.findViewById(R.id.review_layout);
        edtReview = activity.findViewById(R.id.edt_review);
        btnSubmitReview = activity.findViewById(R.id.btn_submit_review);

        btnReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.review();
            }
        });

        boxFindMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.findLocation();
            }
        });

        btnSubmitReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.submitReview();
            }
        });

        reviewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayReviewLayout(false);
            }
        });
    }

    public void displayPlace(Place place) {
        txtTitle.setText(place.getName());
        txtInfo.setText(place.getInfo());
        Picasso.with(activity.getApplicationContext()).load(place.getIcon()).into(imgIcon);
    }

    void customizePager() {
        pager.setAdapter(controller.adapter);
        tabLayout.setupWithViewPager(pager);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                controller.refreshBackdrop(position);
            }

            @Override
            public void onPageSelected(int position) {
                controller.refreshBackdrop(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    void displayReviewButton(boolean show) {
        if (show) btnReview.setVisibility(View.VISIBLE);
        else btnReview.setVisibility(View.GONE);
    }

    void displayReviewLayout(boolean show) {
        if (show) reviewLayout.setVisibility(View.VISIBLE);
        else reviewLayout.setVisibility(View.GONE);
    }

    void customizeRecyclerView() {
        rvReviews.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(activity.getApplicationContext()) {
            @Override
            public boolean canScrollVertically(){
                return false;
            }
        };
        rvReviews.setLayoutManager(llm);
        rvReviews.setAdapter(controller.reviewAdapter);
    }
}
