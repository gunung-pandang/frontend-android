package com.user.gunungpadang.feature.place;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.user.gunungpadang.R;
import com.user.gunungpadang.core.adapter.RecyclerViewAdapter;
import com.user.gunungpadang.feature.place.struct.Review;

import java.util.List;

public class ReviewAdapter extends RecyclerViewAdapter<Review, ReviewAdapter.ViewHolder> {


    public ReviewAdapter(List<Review> list, Context context) {
        super(list, context);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtUsername;
        TextView txtReview;
        ViewHolder(View v) {
            super(v);
            txtUsername = v.findViewById(R.id.txt_username);
            txtReview = v.findViewById(R.id.txt_review);
        }
    }

    @NonNull
    @Override
    public ReviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_review,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewAdapter.ViewHolder holder, int position) {
        final Review review = list.get(position);

        holder.txtUsername.setText(review.getUsername());
        holder.txtReview.setText(review.getReview());
    }
}
