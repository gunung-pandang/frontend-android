package com.user.gunungpadang.feature.register;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.user.gunungpadang.R;

public class RegisterActivity extends AppCompatActivity {

    RegisterController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        controller = new RegisterController(this);
    }
}
