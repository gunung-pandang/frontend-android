package com.user.gunungpadang.feature.register;

import android.app.Activity;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.user.gunungpadang.R;
import com.user.gunungpadang.core.BaseController;
import com.user.gunungpadang.core.Config;
import com.user.gunungpadang.core.model.Account;
import com.user.gunungpadang.feature.home.HomeActivity;

public class RegisterController extends BaseController {

    public RegisterModel model;

    RegisterController(Activity activity) {
        super(activity);
        this.model = new RegisterModel(activity, this);
    }

    private int passwordConfirmed() {
        if (model.getStringFrom(model.edtPassword).length() < Config.minPasswordLength)
            return R.string.must_6_char;
        else if (!model.getStringFrom(model.edtPassword).equals(model.getStringFrom(model.edtConfirmPassword)))
            return R.string.password_must_match;
        return 0;
    }

    public void register() {
        model.onLoadData(model.progressBar, model.btnRegister);
        int id = passwordConfirmed();
        if (id == 0) {
            final Account account = new Account();
            account.setName(model.getStringFrom(model.edtName));
            account.setUsername(model.getStringFrom(model.edtUsername));
            account.setPassword(account.md5(model.getStringFrom(model.edtPassword)));

            account.registerToFirebase(this, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if (databaseError != null) {
                        System.out.println("Data could not be saved " + databaseError.getMessage());
                        model.onFinishData(model.progressBar, model.btnRegister);
                    } else {
                        Toast.makeText(activity, "Account created!", Toast.LENGTH_SHORT).show();
                        if (account.getId() != null) {
                            account.save();
                            goToActivity(HomeActivity.class, true, true);
                        } else {
                            Toast.makeText(activity, "Something was error", Toast.LENGTH_SHORT).show();
                        }
                        model.onFinishData(model.progressBar, model.btnRegister);
                    }
                }
            });
        }
        else {
            Toast.makeText(activity, id, Toast.LENGTH_SHORT).show();
        }
    }
}
