package com.user.gunungpadang.feature.register;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.user.gunungpadang.R;
import com.user.gunungpadang.core.BaseLayoutModel;
import com.user.gunungpadang.feature.login.LoginActivity;

public class RegisterModel extends BaseLayoutModel {

    private RegisterController controller;

    EditText edtName;
    EditText edtUsername;
    EditText edtPassword;
    EditText edtConfirmPassword;
    public Button btnRegister;
    TextView txtLogin;
    public ProgressBar progressBar;

    public RegisterModel(Activity activity, RegisterController controller) {
        super(activity);
        this.controller = controller;
    }

    @Override
    public void render() {
        edtName = activity.findViewById(R.id.edt_name);
        edtUsername = activity.findViewById(R.id.edt_username);
        edtPassword = activity.findViewById(R.id.edt_password);
        edtConfirmPassword = activity.findViewById(R.id.edt_confirm_password);
        btnRegister = activity.findViewById(R.id.btn_register);
        txtLogin = activity.findViewById(R.id.txt_login);
        progressBar = activity.findViewById(R.id.progress_register);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.register();
            }
        });
        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.goToActivity(LoginActivity.class);
            }
        });
    }
}
