package com.user.gunungpadang.feature.splash;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.user.gunungpadang.R;

public class SplashActivity extends AppCompatActivity {

    SplashController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        controller = new SplashController(this);
        controller.startTimeout();
        controller.initLocalDatabase();
    }

    @Override
    protected void onStop() {
        controller.cancel();
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        controller.permissionResponse(requestCode, grantResults);
    }
}
