package com.user.gunungpadang.feature.splash;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.user.gunungpadang.core.BaseController;
import com.user.gunungpadang.core.Config;
import com.user.gunungpadang.core.model.Place;
import com.user.gunungpadang.feature.home.HomeActivity;


class SplashController extends BaseController {

    private SplashModel model;
    private Runnable runnable;
    private Handler handler;

    SplashController(Activity activity) {
        super(activity);
        this.model = new SplashModel(activity);
        handler = new Handler();
    }

    void startTimeout() {
        int SPLASH_TIMEOUT = 2500;
        model.makeFrameDissapear(SPLASH_TIMEOUT);
        runnable = new Runnable() {
            @Override
            public void run() {
                boolean locationAllowed = checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, Config.locationCode);

                if (locationAllowed) {
                    boolean internetAllowed = checkPermission(Manifest.permission.INTERNET, Config.internetCode);
                    if (internetAllowed) {
                        goToActivity(HomeActivity.class, true);
                    }
                }
            }
        };
        handler.postDelayed(runnable, SPLASH_TIMEOUT);
    }

    void cancel() {
        if (runnable != null) {
            handler.removeCallbacks(runnable);
        }
    }


    void initLocalDatabase() {
        FlowManager.init(new FlowConfig.Builder(activity).build());
    }

    void permissionResponse(int requestCode, int[] grantResults) {
        if (requestCode == Config.locationCode){
            if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)){
                Toast.makeText(activity, "Location Allowed", Toast.LENGTH_SHORT).show();
                goToActivity(HomeActivity.class, true);

            }else{
                Toast.makeText(activity, "Permission Denied to access your Location", Toast.LENGTH_SHORT).show();
                activity.finish();
            }
        } else if (requestCode == Config.internetCode) {
            if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)){
                Toast.makeText(activity, "Internet Allowed", Toast.LENGTH_SHORT).show();
                goToActivity(HomeActivity.class, true);

            }else{
                Toast.makeText(activity, "Permission Denied to access your Internet", Toast.LENGTH_SHORT).show();
                activity.finish();
            }
        }
    }
}
