package com.user.gunungpadang.feature.splash;

import android.app.Activity;
import android.widget.LinearLayout;

import com.user.gunungpadang.R;
import com.user.gunungpadang.core.BaseLayoutModel;

public class SplashModel extends BaseLayoutModel {
    private LinearLayout frameIcon;

    SplashModel(Activity activity) {
        super(activity);
    }

    @Override
    public void render() {
        frameIcon = activity.findViewById(R.id.frame_icon);
    }

    void makeFrameDissapear(long duration) {
        frameIcon.animate().alpha(0f).setDuration(duration);
    }


}
